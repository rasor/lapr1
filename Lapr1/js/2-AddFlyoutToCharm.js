(function () {
    "use strict";
    //var page = WinJS.UI.Pages.define("/html/2-AddFlyoutToCharm.html", {
    //    ready: function (element, options) {
    //        //document.getElementById("scenario2Add").addEventListener("click", scenario2AddSettingsFlyout, false);
    //        document.getElementById("stateButton").addEventListener("click", scenarioShowSettingsFlyout, false);

    //        // clear out the current on settings handler to ensure scenarios are atomic
    //        //WinJS.Application.onsettings = null;

    //        // Display invocation instructions in the SDK sample output region
    //        //WinJS.log && WinJS.log("To show the settings charm, invoke the charm bar by swiping your finger on the right edge of the screen or bringing your mouse to the lower-right corner of the screen, then select Settings. Or you can just press Windows logo + i. To dismiss the settings charm, tap in the application, swipe a screen edge, right click, invoke another charm or application.", "sample", "status");

    //    }
    //});

    function scenarioAddSettingsFlyout() {
        WinJS.Application.onsettings = function (e) {
            //e.detail.applicationcommands = { "help": { title: "Help", href: "/html/2-SettingsFlyout-Help.html" } };
            e.detail.applicationcommands = {
                "helpView": { title: "Help", href: "/html/2-SettingsFlyout-Help.html" },
                "settingsView": { title: "LapR Settings", href: "/html/5-SettingsFlyout-Settings.html" }
            };
            WinJS.UI.SettingsFlyout.populateSettings(e);
        };
        // Make sure the following is called after the DOM has initialized. Typically this would be part of app initialization
        //WinJS.Application.start();

        // Display a status message in the SDK sample output region
        //WinJS.log && WinJS.log("Help command and settings flyout added from 2-SettingsFlyout-Help.html", "samples", "status");
    }
    //Add help to settings
    scenarioAddSettingsFlyout();

    function _onShowHelpClick(e) {
        WinJS.UI.SettingsFlyout.showSettings("helpView", "/html/2-SettingsFlyout-Help.html");

        // Display a status message in the SDK sample output region
        //WinJS.log && WinJS.log("Help settings flyout showing", "samples", "status");
    }
    //Export scenarioShowSettingsFlyout() as laprHelp.onShowHelpClick
    var namespacePublicMembers = { onShowHelpClick: _onShowHelpClick };
    WinJS.Namespace.define("laprHelp", namespacePublicMembers);

})();
