﻿(function () {
    "use strict";

    var localSettings = Windows.Storage.ApplicationData.current.localSettings;

    //Replaces body onload
    var page = WinJS.UI.Pages.define("/html/5-SettingsFlyout-Settings.html", {
        ready: function (element, options) {
            var txtinput;
            var intVal;
            var toggle;
            var boolVal;

            //lapThresholdTime
            txtinput = document.getElementById("lapThresholdTimeInput");
            intVal = localSettings.values["lapThresholdTime"];
            intVal = (intVal && !isNaN(intVal)) ? intVal : 44; //44 if val is undef or NaN
            txtinput.value = intVal; //restore from settings
            txtinput.addEventListener("change",
                function (e) {
                    intVal = parseInt(e.target.value);
                    if (!isNaN(intVal)) {
                        WinJS.log && WinJS.log('', 'samples', 'status')
                        localSettings.values["lapThresholdTime"] = intVal; //backup to settings
                        LaprDefault.onThresholdTimeChanged(intVal); //do updates on startPage
                    } else {
                        WinJS.log && WinJS.log('Threshold Time must be a number', 'samples', 'error')
                    }
                }
            );

            //lastLap
            txtinput = document.getElementById("lastLapInput");
            intVal = localSettings.values["lastLap"];
            intVal = (intVal && !isNaN(intVal)) ? intVal : 2; //2 if val is undef or NaN
            txtinput.value = intVal; //restore from settings
            txtinput.addEventListener("change",
                function (e) {
                    intVal = parseInt(e.target.value);
                    if (!isNaN(intVal)) {
                        WinJS.log && WinJS.log('', 'samples', 'status')
                        localSettings.values["lastLap"] = intVal; //backup to settings
                        LaprDefault.onMaxLapsChanged(intVal); //do updates on startPage
                    } else {
                        WinJS.log && WinJS.log('Last Lap must be a number', 'samples', 'error')
                    }
                }
            );

            //useInternetMaster
            toggle = document.getElementById("useInternetMasterInput").winControl;
            boolVal = localSettings.values["useInternetMaster"];
            boolVal = !boolVal ? false : boolVal; //false if val is undef
            toggle.checked = boolVal; //restore from settings
            toggle.addEventListener("change", 
                function (e) {
                    boolVal = e.target.winControl.checked;
                    localSettings.values["useInternetMaster"] = boolVal; //backup to settings
                    LaprDefault.onUseInternetMasterChanged(boolVal); //do updates on startPage
                }
            );
        }
    });

    //http://rindudendam.wordpress.com/2008/10/28/javascript-isarray-isboolean-ishtmlelement-isnull-isnumber-isobject-isstring-isundefined-isvalue-ltrim-rtrim-trim-dalam-satu-package/
    function isBoolean(o) {
        return typeof o === 'boolean' ;
    }

})();