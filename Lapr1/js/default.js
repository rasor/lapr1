﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232509

// Global vars
var useInternetMaster;

(function () {
    "use strict";

    WinJS.Binding.optimizeBindingReferences = true;

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;
    var localSettings = Windows.Storage.ApplicationData.current.localSettings;

    //LapR code
    WinJS.strictProcessing();
    function Enum() {}  
    Enum.AppStates = { "notready": 0, "ready": 1, "running": 2, "stopped": 3, "uninitilized": 9 };
    Enum.DistanceIn = { "Miles": 0, "Kilometers": 1 }; 
    var appState = Enum.AppStates.uninitilized;
    var lastPosition = null;
    var startPosition = null;
    var alertTimerId = 0;
    var totSeconds = 0;
    var lapSeconds = 0;
    var maxlaps;// = 2;
    var donelaps = 0;
    var lapThresholdTime;// = 44;
    var ticked = false;
    var starting = false;

    var gl = new Windows.Devices.Geolocation.Geolocator();
    //var reportInterval = gl.reportInterval; //0

    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: This application has been newly launched. Initialize
                // your application here.
            } else {
                // TODO: This application has been reactivated from suspension.
                // Restore application state here.
            }
            //args.setPromise(WinJS.UI.processAll());
            args.setPromise(WinJS.UI.processAll().done(function () {

                //LapR code
                //Wire header buttons
                document.getElementById("scenarioPlayButton").addEventListener("click", onPlayClick, false);
                document.getElementById("stateButton").addEventListener("click", laprHelp.onShowHelpClick, false);
                //Wire AppBar buttons:
                //Play button is reused for play and pause
                document.getElementById("cmdPlay").addEventListener("click", onPlayClick, false);
                document.getElementById("cmdReset").addEventListener("click", onResetClick, false);

                //document.getElementById("maxLaps").innerText = localSettings.values["lastLap"];

                //restore lastLap
                var intVal = localSettings.values["lastLap"];
                intVal = (intVal && !isNaN(intVal)) ? intVal : 2; //2 if val is undef or NaN
                _onMaxLapsChanged(intVal);

                //restore lapThresholdTime
                intVal = localSettings.values["lapThresholdTime"];
                intVal = (intVal && !isNaN(intVal)) ? intVal : 44; //44 if val is undef or NaN
                _onThresholdTimeChanged(intVal);

                //restore useInternetMaster
                var boolVal = localSettings.values["useInternetMaster"];
                useInternetMaster = !boolVal ? false : boolVal; //false if val is undef
                setMapSrc();

                //Initial state
                setNotReadyState();

                //wire gps status handler
                gl.addEventListener("statuschanged", onPositionStatusChanged, false);

                if (gl.locationStatus === Windows.Devices.Geolocation.PositionStatus.ready) {
                    setReadyState();
                } else {
                    //http://www.tozon.info/blog/post/2011/09/15/Windows-8-geolocation-API-First-run.aspx
                    setNotReadyState();
                }

                //Test gps
                //var here = {
                //    latitude: 53.470818,
                //    longitude: 11.806896
                //};
                //var there = {
                //    latitude: 53.470861,
                //    longitude: 11.806886
                //};
                //var dist = distance(Enum.DistanceIn.Kilometers, here, there) * 1000;
            })
            );

        }
    };

    app.oncheckpoint = function (args) {
        // TODO: This application is about to be suspended. Save any state
        // that needs to persist across suspensions here. You might use the
        // WinJS.Application.sessionState object, which is automatically
        // saved and restored across suspension. If you need to complete an
        // asynchronous operation before your application is suspended, call
        // args.setPromise().
    };

    //LapR code

    //Functions *****************************************
    //For Map
    function callFrameScript(frame, targetFunction, args) {
        var message = { functionName: targetFunction, args: args };
        frame.postMessage(JSON.stringify(message), "ms-appx-web://" + document.location.host);
    }
    function processFrameEvent(message) {
        //Verify data and origin (in this case the web context of the app)
        if (!message.data || message.origin !== "ms-appx-web://" + document.location.host) {
            return;
        }

        var eventObj = JSON.parse(message.data);

        switch (eventObj.event) {
            case "locationChanged":
                lastPosition = { latitude: eventObj.latitude, longitude: eventObj.longitude };
                break;

            default:
                break;
        }
    }

    //http://blogs.msdn.com/b/dragoman/archive/2010/09/29/wp7-code-distance-computations-with-the-geolocation-api.aspx
    function distance(distin, here, there) { 
        var r = (distin === Enum.DistanceIn.Miles) ? 3960 : 6371; 
        var dLat = ToRadian(there.latitude - here.latitude);
        var dLon = ToRadian(there.longitude - here.longitude);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + 
                Math.cos(ToRadian(here.latitude)) * Math.cos(ToRadian(there.latitude)) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2); 
        var c = 2 * Math.asin(Math.min(1, Math.sqrt(a))); 
        var d = r * c; 
        return d; 
    } 
    function ToRadian(val) { 
        return (Math.PI / 180) * val; 
    } 

    function setAppBarButton() {
        var btn = document.getElementById("cmdPlay");
        switch (appState) {
            case Enum.AppStates.ready:
            case Enum.AppStates.stopped:
                btn.winControl.icon = "play";
                btn.winControl.label = "play";
                btn.winControl.tooltip = "Start the race";
                btn.style.visibility = "visible";
                break;
            case Enum.AppStates.running:
                btn.winControl.icon = "pause";
                btn.winControl.label = "pause";
                btn.winControl.tooltip = "Stop the race";
                btn.style.visibility = "visible";
                break;
            case Enum.AppStates.notready:
                btn.style.visibility = "hidden";
                break;
            default:
                break;
        }
        var btnr = document.getElementById("cmdReset");
        switch (appState) {
            case Enum.AppStates.stopped:
                //btnr.winControl.hidden = "false";
                btnr.style.visibility = "visible";
                break;
            default:
                //btnr.winControl.hidden = "true";
                btnr.style.visibility = "hidden";
                break;
        }
    }
    function setNotReadyState() {
        if (appState !== Enum.AppStates.notready) {
            appState = Enum.AppStates.notready;
            document.getElementById("stateButton").className = 'xtra-notreadybutton';
            document.getElementById("stateButton").title = "Not Ready";
            document.getElementById("scenarioPlayButton").style.visibility = 'hidden';

            //Hide start button in appBar
            setAppBarButton();
       }
    }
    function setReadyState() {
        appState = Enum.AppStates.ready;
        document.getElementById("stateButton").className = 'xtra-readybutton';
        document.getElementById("stateButton").title = "Ready";
        document.getElementById("scenarioPlayButton").style.visibility = 'visible';

        //Change start button to play button in appBar
        setAppBarButton();
    }
    function setRunningState() {
        appState = Enum.AppStates.running;
        document.getElementById("stateButton").className = 'xtra-runningbutton';
        document.getElementById("stateButton").title = "Running";
        document.getElementById("scenarioPlayButton").style.visibility = 'hidden';

        //Change start button to pause button in appBar
        setAppBarButton();
    }
    function setStoppedState() {
        appState = Enum.AppStates.stopped;
        document.getElementById("stateButton").className = 'xtra-stoppedbutton';
        document.getElementById("stateButton").title = "Stopped";
        document.getElementById("scenarioPlayButton").style.visibility = 'visible';

        //Change start button to start button in appBar
        setAppBarButton();

        //document.getElementById("scenarioFullscreen").style.visibility = 'visible';
        // Show the AppBar in full screen mode
        //document.getElementById("scenarioAppBar").winControl.disabled = false;
    }
    function pad2(number) {
        var retVal = ("0" + number).slice(-2);
        return retVal;
    }
    function secsToTime(seconds) {
        var secs = seconds % 60;
        var minuts = (seconds - secs) / 60;
        var mins = minuts % 60;
        var hours = (minuts - mins) / 60;
        return "" + hours + ":" + pad2(mins) + ":" + pad2(secs);
    }
    function renderLaptime() {
        document.getElementById("lapTime").innerText = secsToTime(lapSeconds);
    }
    function renderTottime() {
        document.getElementById("totTime").innerText = secsToTime(totSeconds);
    }
    function renderDoneLaps() {
        document.getElementById("doneLaps").innerText = donelaps;
    }
    function renderMaxLaps() {
        document.getElementById("maxLaps").innerText = maxlaps;
    }
    function getGpsPosition() {
        
        //http://www.tozon.info/blog/post/2011/09/15/Windows-8-geolocation-API-First-run.aspx
        gl.getGeopositionAsync().done(function (position) {
            //Save for share
            lastPosition = {
                latitude: position.coordinate.latitude,
                longitude: position.coordinate.longitude
            };

            //callFrameScript(document.frames["map"], "pinLocation",
            //    [position.coordinate.latitude, position.coordinate.longitude]);

            switch (appState) {
                case Enum.AppStates.running:
                    startingRaceAwaited();
                    break;
                default:
                    break;
            }

            if (ticked) {
                //Toggle back
                ticked = false;
                //IfStartReachedThenIncrementLapsAndResetCurrLapTime
                //Get distance in meters
                var dist = distance(Enum.DistanceIn.Kilometers, lastPosition, startPosition) * 1000;
                //if less than 15 m from start, then one lap is reached
                var startReached = dist < 15;

                if (startReached) {
                    lapSeconds = 0;
                    renderLaptime();

                    donelaps++;
                    renderDoneLaps();

                    //stop race if no more laps to do
                    if (donelaps === maxlaps) {
                        onStoppingRace();
                    }
                }
            }
        }, function (error) {
            console.log("Unable to get GPS location.");
        });
    }
    function startingRaceAwaited() {
        if (starting) {
            starting = false;

            //After Got GPS position Async
            startPosition = lastPosition;

            //Render startPosition
            document.getElementById("startPosition").innerText = "Startposition: (latitude, longitude) " + startPosition.latitude + ", " + startPosition.longitude;

            callFrameScript(document.frames["map"], "pinLocation",
                [lastPosition.latitude, lastPosition.longitude]);
        }
    }
    //Eventhandlers *****************************************
    function onPositionStatusChanged() {
        console.log("onPositionStatusChanged");
        if (gl.locationStatus === Windows.Devices.Geolocation.PositionStatus.ready) {
            if (appState === Enum.AppStates.notready) {
                setReadyState();
            }
        }

    }
    function onPlayClick() {
        switch (appState) {
            case Enum.AppStates.ready:
                onStartingRace();
                break;
            case Enum.AppStates.running:
                onStoppingRace();
                break;
            case Enum.AppStates.stopped:
                onContinuingRace();
                break;
            default:
                break;
        }
    }
    function onResetClick() {
        switch (appState) {
            case Enum.AppStates.stopped:
                onResettingRace();
                break;
            default:
                break;
        }
    }
    function onStartingRace() {
        if (appState === Enum.AppStates.ready) {
            starting = true;
            //Start timer and let it send an event each x second
            // http://www.elated.com/articles/javascript-timers-with-settimeout-and-setinterval/
            alertTimerId = self.setInterval(function () { onTimerInterval(); }, 1000);

            //Set state and show state
            setRunningState();

            //Render timers to 0
            totSeconds = 0;
            lapSeconds = 0;
            renderTottime();
            renderLaptime();

            //Get GPS position Async
            getGpsPosition();
            //Code hereafter will be in parrallel with above

            WinJS.log && WinJS.log('Race Started', 'samples', 'status')
        }
    }
    function onStoppingRace() {
        if (appState === Enum.AppStates.running) {
            //Stop timer
            clearInterval(alertTimerId);

            //Set state and show state
            setStoppedState();

            //Get GPS position Async
            //getGpsPosition();
            //Code hereafter will be in parrallel with above

            //Capture goal photo
        }
    }
    function onContinuingRace() {
        if (appState === Enum.AppStates.stopped) {
            //Set state and show state
            setRunningState();

            //Continue timer
            alertTimerId = self.setInterval(function () { onTimerInterval(); }, 1000);

            //Get GPS position Async
            getGpsPosition();
            //Code hereafter will be in parrallel with above
        }
    }
    function onResettingRace() {
        if (appState === Enum.AppStates.stopped) {
            //Set state and show state
            setReadyState();

            //Render timers to 0
            totSeconds = 0;
            lapSeconds = 0;
            donelaps = 0;
            renderTottime();
            renderLaptime();
            renderDoneLaps();
        }
    }
    function onTimerInterval() {
        //Increase our time parser
        totSeconds++;
        lapSeconds++;
        renderTottime();
        renderLaptime();

        //A lap must be minimum 45 sec
        if (lapSeconds > lapThresholdTime) {
            ticked = true;
            //Get GPS position Async
            getGpsPosition();
            //Code hereafter will be in parrallel with above

            //IfStartReachedThenIncrementLapsAndResetCurrLapTime - call async after got GPS
        }
    }
    function _onThresholdTimeChanged(newValue) {
        lapThresholdTime = newValue;
    }
    function _onMaxLapsChanged(newValue) {
        maxlaps = newValue;
        renderMaxLaps();
    }
    function _onUseInternetMasterChanged(newValue) {
        useInternetMaster = newValue;
        setMapSrc();
    }
    function setMapSrc() {
        if (useInternetMaster) {
            document.getElementById("map").src = "ms-appx-web:///html/map.html";
        } else {
            document.getElementById("map").src = "#";
        }
    }

    //Export functions as LaprDefault.onMaxLapsChanged
    var namespacePublicMembers = {
        onMaxLapsChanged: _onMaxLapsChanged,
        onThresholdTimeChanged: _onThresholdTimeChanged,
        onUseInternetMasterChanged: _onUseInternetMasterChanged
    };
    WinJS.Namespace.define("LaprDefault", namespacePublicMembers);

    app.start();
})();
